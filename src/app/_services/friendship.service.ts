import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/friendship/';

@Injectable({
  providedIn: 'root'
})
export class FriendshipService {
  constructor(private http: HttpClient) { }


  getFriendsList(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }

  addFriend(id: Number): Observable<any> {
    return this.http.post(API_URL + id, { responseType: 'json' });
  }
  removeFriend(id: Number): Observable<any> {
    return this.http.delete(API_URL + id, { responseType: 'json' });
  }

}