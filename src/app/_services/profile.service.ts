import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/me/';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  getUserInfo(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }
  createUserInfo(userInfo: any): Observable<any> {
    return this.http.post(API_URL, userInfo, { responseType: 'json' });
  }
  updateUserInfo(userInfo: any, id: any): Observable<any> {
    return this.http.put(API_URL + id, userInfo, { responseType: 'json' });
  }
  getEnrolledCourses(): Observable<any> {
    return this.http.get(API_URL + 'enrolled/', { responseType: 'json' });
  }
}
