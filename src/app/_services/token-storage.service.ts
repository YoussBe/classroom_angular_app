import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const USERINFO_KEY = 'auth-user-info';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    console.log("user :", JSON.stringify(user))
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }
  public saveUserInfo(user: any, userInfo: any): void {
    console.log(user.type);
    if (user.type == 1) {
      userInfo.type = 1;
    } else {
      userInfo.type = 2;
    }
    window.sessionStorage.removeItem(USERINFO_KEY);
    console.log("userInfo :", JSON.stringify(userInfo))
    window.sessionStorage.setItem(USERINFO_KEY, JSON.stringify(userInfo));
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }
  public getUserInfo(): any {
    const userInfo = window.sessionStorage.getItem(USERINFO_KEY);
    if (userInfo) {
      return JSON.parse(userInfo);
    }

    return {};
  }
}