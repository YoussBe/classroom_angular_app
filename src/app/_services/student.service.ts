import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/students/';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  constructor(private http: HttpClient) { }


  getStudentsList(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }

  getStudentById(id: number): Observable<any> {
    return this.http.get(API_URL + id, { responseType: 'json' });
  }
  enrollToLesson(id: number): Observable<any> {
    return this.http.post(API_URL + 'enroll/' + id, { responseType: 'json' });
  }

}