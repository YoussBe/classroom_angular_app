import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/publications/';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {
  constructor(private http: HttpClient) { }


  getPublicationsList(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }
  createPublication(publication: any): Observable<any> {
    console.log(publication);
    return this.http.post(API_URL, publication, { responseType: 'json' });
  }

}