import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/lessons/';

@Injectable({
  providedIn: 'root'
})
export class LessonService {
  constructor(private http: HttpClient) { }


  getLessonsList(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }

  getLessonById(id: number): Observable<any> {
    return this.http.get(API_URL + id, { responseType: 'json' });
  }
  createLessons(lesson: any): Observable<any> {
    return this.http.post(API_URL, lesson, { responseType: 'json' });
  }

}