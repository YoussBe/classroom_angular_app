import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/comments/';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private http: HttpClient) { }


  getCommentsByPublication(id: Number): Observable<any> {
    return this.http.get(API_URL + 'publication/' + id, { responseType: 'json' });
  }

  addCommentinPublication(id: Number, comment: any): Observable<any> {
    return this.http.post(API_URL + 'publication/' + id, comment, { responseType: 'json' });
  }

}