import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:3000/teachers/';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  constructor(private http: HttpClient) { }


  getTeachersList(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'json' });
  }
  getTeacherById(id: number): Observable<any> {
    return this.http.get(API_URL + id, { responseType: 'json' });
  }

}