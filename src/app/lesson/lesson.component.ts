import { Component, OnInit } from '@angular/core';
import { LessonService } from '../_services/lesson.service';
import { ProfileService } from '../_services/profile.service';
import { StudentService } from '../_services/student.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.css']
})
export class LessonComponent implements OnInit {

  constructor(private lessonService: LessonService, private profileService: ProfileService, private studentService: StudentService, private token: TokenStorageService,) { }

  lessons?: any = [];
  currentUser: any;
  currentUserInfo: any;

  enrolledLessons: any = [];
  form: any = {
    title: null,
    hours: null,
    description: null,
    file_name: null,
    starting_date: null,
    ending_date: null
  };


  navigate(url: string): void {
    window.location.href = url;

  }

  ngOnInit(): void {
    this.currentUserInfo = this.token.getUserInfo();

    this.lessonService.getLessonsList().subscribe(
      data => {
        this.lessons = data;
        this.currentUserInfo.type == 1 ?
          this.profileService.getEnrolledCourses().subscribe(data => {
            this.enrolledLessons = data;
            this.lessons.map((element?: any) => {
              this.enrolledLessons.find((enrolledElement?: any) => enrolledElement.id == element.id) != null ? element.isEnrolled = true : element.isEnrolled = false;

            });

          }) : this.enrolledLessons = [];
      },
      err => { });

    this.profileService.getUserInfo().subscribe(data => {
      this.currentUser = data;
      console.log(data);

    })


  }
  onSubmit(): void {
    this.lessonService.createLessons(this.form).subscribe(
      data => {
        this.lessons.unshift(data);

        this.form = {
          title: null,
          hours: null,
          description: null,
          file_name: null,
          starting_date: null,
          ending_date: null
        };
      },
      err => { });
  }

  onEnroll(index: number): void {
    this.studentService.enrollToLesson(this.lessons[index].id).subscribe(data => {
      this.lessons[index].isEnrolled = true;
    });
  }



}
