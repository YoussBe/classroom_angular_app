import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../_services/profile.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: any;
  form: any = {
    first_name: null,
    last_name: null,
    bio: null,
    level: null,
    birthdate: null,
    profile_picture: null
  };

  constructor(private profileService: ProfileService, private storage: TokenStorageService) { }

  ngOnInit(): void {
    this.profileService.getUserInfo().subscribe(data => {
      this.user = data;

      console.log(data);
      this.form = {
        first_name: data.userInfo.first_name,
        last_name: data.userInfo.last_name,
        bio: data.userInfo.bio,
        level: data.userInfo.level,
        birthdate: new Date(data.userInfo.birthdate),
        profile_picture: data.userInfo.profile_picture
      }
    })

  }

  onSubmit(): void {
    if (this.user.userInfo) {
      this.profileService.updateUserInfo(this.form, this.user.userInfo.id).subscribe(data => {
        console.log(data);
        this.user = data;
        console.log(this.user);
        this.storage.saveUserInfo(this.user.user, this.user.userInfo);
      })
    } else {
      this.profileService.createUserInfo(this.form).subscribe(data => {
        this.user = data;
        console.log(this.user);
        this.storage.saveUserInfo(this.user.user, this.user.userInfo);
        console.log(data);

      })

    }

  }
}