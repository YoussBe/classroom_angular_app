import { Component, OnInit } from '@angular/core';
import { StudentService } from '../_services/student.service';
import { FriendshipService } from '../_services/friendship.service';

import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  students?: any;
  currentUserInfo: any;
  friends?: any;

  constructor(private studentService: StudentService, private token: TokenStorageService, private friendshipService: FriendshipService) { }


  ngOnInit(): void {
    this.currentUserInfo = this.token.getUserInfo();
    console.log(this.currentUserInfo);
    this.studentService.getStudentsList().subscribe(
      data => {
        this.students = data;
        this.friendshipService.getFriendsList().subscribe(
          data => {
            this.friends = data;
            this.students.map((element?: any) => {
              this.friends.find((friendelement?: any) => friendelement.id == element.id) != null ? element.isFriend = true : element.isFriend = false;

            });
          }
        )

        console.log(this.students);
      },
      err => { });
  }

  addFriendship(index: number): void {
    this.friendshipService.addFriend(this.students[index].id).subscribe(
      data => {
        this.students[index].isFriend = true;
      }
    )

  }
  endFriendship(index: number): void {
    this.friendshipService.removeFriend(this.students[index].id).subscribe(
      data => {
        this.students[index].isFriend = false;
      }
    )
  }
}
