import { Component, OnInit } from '@angular/core';
import { PublicationService } from '../_services/publication.service';
import { LessonService } from '../_services/lesson.service';

import { CommentService } from '../_services/comment.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { StudentService } from '../_services/student.service';
import { TeacherService } from '../_services/teacher.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  content?: any;
  currentUserInfo: any = null;

  form: any = {
    title: null,
    body_text: null,
    type: 0,
    lesson: null,
  };

  activeCommentpub?: Number;
  lessons?: any;

  commentForm: any = {
    title: null,
    body_text: null,
  };

  constructor(private pubService: PublicationService, private token: TokenStorageService, private commentService: CommentService, private lessonService: LessonService, private studentService: StudentService, private teacherService: TeacherService) { }

  ngOnInit(): void {
    this.currentUserInfo = this.token.getUserInfo();
    console.log(this.currentUserInfo);

    this.pubService.getPublicationsList().subscribe(
      data => {
        data = data.reverse();
        console.log(data);
        this.content = data;
        data.map((pub: any) => {
          this.commentService.getCommentsByPublication(pub.id).subscribe(
            data2 => {
              data2 = data2.reverse();
              pub.comments = data2;
            },
            err => {
              console.log(err);
            }
          );
          if (pub.studentId) {
            this.studentService.getStudentById(pub.studentId).subscribe(
              data => {
                pub.author = data;
              }
            );
          } else {
            this.teacherService.getTeacherById(pub.teacherId).subscribe(
              data => {
                pub.author = data;
              }
            );;
          }

        }),
          data.map((pub: any) => {
            if (pub.lessonId) {
              this.lessonService.getLessonById(pub.lessonId).subscribe(
                data3 => {
                  pub.lesson = data3;
                },
                err => {
                  console.log(err);
                }
              )
            }

          })
      },
      err => {
        console.log(err);
        this.content = JSON.parse(err).message;
      }
    );

    this.lessonService.getLessonsList().subscribe(
      data => {
        this.lessons = data;
      }
    )
  }

  navigate(url: string): void {
    window.location.href = url;

  }

  changeLesson(e: any) {
    this.form.lesson = e.target.value
    console.log(e);
    console.log(this.form);
  }


  onPubSubmit(): void {
    let body = {
      publication: this.form,
      lesson: this.form.lesson,
    }
    this.pubService.createPublication(body).subscribe(data => {
      console.log(data);

      let entry: any;
      entry = data.publication;
      entry.lesson = data.lesson;
      console.log(entry);
      this.content.unshift(entry);
      this.form = {
        title: null,
        body_text: null,
        type: 0,
      };



    })


  }

  setPubForComment(index: Number) {
    this.activeCommentpub = index;
  }


  onCommentSubmit(id: Number): void {
    this.commentService.addCommentinPublication(id, this.commentForm).subscribe(data => {
      window.location.reload();
      this.form = {
        title: null,
        body_text: null,
        type: 0,
      };

    })


  }
}